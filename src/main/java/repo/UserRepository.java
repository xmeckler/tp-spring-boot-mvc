package repo;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import model.User;

public interface UserRepository extends CrudRepository<User, Long> {
	public List<User> findAllByOrderBySubscriptionDateAsc();
	public List<User> findAllByOrderBySubscriptionDateDesc();
	public List<User> findAllByOrderByRaisonSocialeAsc();
	public List<User> findAllByOrderByRaisonSocialeDesc();
	public User findOneBySiretNumber(String siret);
}
