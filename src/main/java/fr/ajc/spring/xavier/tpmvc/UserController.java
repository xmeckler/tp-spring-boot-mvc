package fr.ajc.spring.xavier.tpmvc;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import model.User;
import repo.UserRepository;

@Controller
public class UserController {
	@Value("${spring.application.name}")
	String appName;
	
	@Autowired
	private UserRepository userRepository;
	
	@GetMapping("/")
	public String homePage(Model model) {
		model.addAttribute("appName", appName);
		String encoded = new BCryptPasswordEncoder().encode("password");
		model.addAttribute("encoded", encoded);
		return "home";
	}
	
	@GetMapping("/user/new")
    public String newUserForm(Model model) {
        model.addAttribute("user", new User());
        return "userForm";
    }

    @PostMapping("/user/new")
    public String newUserFormSubmit(@Valid @ModelAttribute User user, BindingResult bindingResult, HttpServletResponse response) {
    	if (bindingResult.hasErrors()) {
    		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            return "userForm";
        }
    	userRepository.save(user);
    	response.setStatus(HttpServletResponse.SC_CREATED);
        return "success";
    }
}
