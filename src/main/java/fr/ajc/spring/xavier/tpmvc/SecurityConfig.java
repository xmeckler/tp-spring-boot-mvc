package fr.ajc.spring.xavier.tpmvc;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
		auth.inMemoryAuthentication()
		.withUser("api_consumer")
		.password(encoder.encode("api_secret"))
		.roles("USER")
		.and()
		.withUser("superman")
		.password(encoder.encode("krypton"))
		.roles("ADMIN");
	}
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		.antMatchers("/api/users")
		.authenticated()
		.antMatchers("/")
		.permitAll()
		.and()
		.httpBasic()
		.and()
		.formLogin()
		.and()
		.logout()
		.logoutRequestMatcher(new AntPathRequestMatcher("/deconnexion"))
		.logoutSuccessUrl("/")
		.and()
		.csrf()
		.disable();
	}
}
