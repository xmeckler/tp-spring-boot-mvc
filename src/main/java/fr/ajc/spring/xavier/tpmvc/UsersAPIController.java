package fr.ajc.spring.xavier.tpmvc;

import java.util.ArrayList;
import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import model.User;
import model.UserExposed;
import repo.UserRepository;


@RestController
@RequestMapping("/api/users")
public class UsersAPIController {
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(
			value = "",
			produces = { "application/json", "application/xml" },
			method = RequestMethod.GET)
	public @ResponseBody List<UserExposed> findAll(@RequestParam(value="sort", required=false) String sort, @RequestParam(value="order", required=false) String order) {
		List<User> usersList = new ArrayList<User>();
		if (sort != null || order != null) {
			if (sort.equals("byDate") && order.equals("ASC")) {
				usersList = userRepository.findAllByOrderBySubscriptionDateAsc();
			} else if (sort.equals("byName") && order.equals("ASC")) {
				usersList = userRepository.findAllByOrderByRaisonSocialeAsc();
			} else if (sort.equals("byDate") && order.equals("DESC")) {
				usersList = userRepository.findAllByOrderBySubscriptionDateDesc();
			} else if (sort.equals("byName") && order.equals("DESC")) {
				usersList = userRepository.findAllByOrderByRaisonSocialeDesc();
			}
		} else {
			usersList = (List<User>) userRepository.findAll();
		}
		List<UserExposed> exposedList = new ArrayList<UserExposed>();
		for(User user : usersList) {
			UserExposed expUser = new UserExposed(user);
			exposedList.add(expUser);
		}
		return exposedList;
	}
	
	@RequestMapping(
			value = "/{siret}",
			produces = { "application/json", "application/xml" },
			method = RequestMethod.GET)
	public @ResponseBody UserExposed findOneBySiretNumber(@PathVariable String siret) {
		try {
			User user = userRepository.findOneBySiretNumber(siret);
			UserExposed expUser = new UserExposed(user);
			return expUser;
	    } catch (NullPointerException ex) {
	        throw new ResponseStatusException(
	          HttpStatus.NOT_FOUND, "Utilisateur introuvable", ex);
	    }
	}
}
