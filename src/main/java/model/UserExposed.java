package model;

import java.time.LocalDate;

public class UserExposed {
	
	private long id;
	
	private String raisonSociale;

	private String numTVAIntracom;
	
	private String siretNumber;
	
	private int employeeNumber;
	
	private boolean newsLetterSubscribed;
	
	private LocalDate subscriptionDate;

	public UserExposed(User user) {
		this.id = user.getId();
		this.raisonSociale = user.getRaisonSociale();
		this.numTVAIntracom = user.getNumTVAIntracom();
		this.siretNumber = user.getSiretNumber();
		this.employeeNumber = user.getEmployeeNumber();
		this.newsLetterSubscribed = user.isNewsLetterSubscribed();
		this.subscriptionDate = user.getSubscriptionDate();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getNumTVAIntracom() {
		return numTVAIntracom;
	}

	public void setNumTVAIntracom(String numTVAIntracom) {
		this.numTVAIntracom = numTVAIntracom;
	}

	public String getSiretNumber() {
		return siretNumber;
	}

	public void setSiretNumber(String siretNumber) {
		this.siretNumber = siretNumber;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public boolean isNewsLetterSubscribed() {
		return newsLetterSubscribed;
	}

	public void setNewsLetterSubscribed(boolean newsLetterSubscribed) {
		this.newsLetterSubscribed = newsLetterSubscribed;
	}

	public LocalDate getSubscriptionDate() {
		return subscriptionDate;
	}

	public void setSubscriptionDate(LocalDate subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}

	@Override
	public String toString() {
		return "UserExposed [raisonSociale=" + raisonSociale + ", numTVAIntracom=" + numTVAIntracom + ", siretNumber="
				+ siretNumber + "]";
	}
	
	
	
}
