package model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	
	@NotBlank(message="La raison sociale doit être renseignée")
	@Column(nullable = false)
	private String raisonSociale;
	
	@Email
	@NotBlank(message="L'email doit être renseigné")
	@Column(nullable = false)
	private String email;
	
	
	@Pattern(regexp="^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$", message="Le mot de passe doit contenir au moins un chiffre, une lettre minuscule et une lettre majuscule")
	@NotBlank(message="Le mot de passe ne peut être vide")
	@Column(nullable = false)
	private String password;
	
	@Pattern(regexp="[A-Z]{2}[A-Z0-9]{2}[0-9]{9}", message="doit respecter le format: FR40123456824 ")
	@Column(nullable = false)
	private String numTVAIntracom;
	
	@NotNull(message="Le numéro SIRET doit être renseigné")
	@Pattern(regexp="[0-9]{14}", message="doit respecter le format: 12345678912345")
	@Column(nullable = false)
	private String siretNumber;
	
	@Min(5)
	@Column(nullable = true)
	private int employeeNumber;
	
	private boolean newsLetterSubscribed;
	
	private LocalDate subscriptionDate;

	public User(@NotBlank(message = "La raison sociale doit être renseignée") String raisonSociale,
			@Email @NotBlank(message = "L'email doit être renseigné") String email,
			@Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,}$", message = "Le mot de passe doit contenir au moins un chiffre, une lettre minuscule et une lettre majuscule") @NotBlank(message = "Le mot de passe ne peut être vide") String password,
			@Pattern(regexp = "[A-Z]{2}[A-Z0-9]{2}[0-9]{9}", message = "doit respecter le format: FR40123456824 ") String numTVAIntracom,
			@NotNull(message = "Le numéro SIRET doit être renseigné") @Pattern(regexp = "[0-9]{14}", message = "doit respecter le format: 12345678912345") String siretNumber,
			@Min(5) int employeeNumber, boolean newsLetterSubscribed) {
		this.raisonSociale = raisonSociale;
		this.email = email;
		this.password = password;
		this.numTVAIntracom = numTVAIntracom;
		this.siretNumber = siretNumber;
		this.employeeNumber = employeeNumber;
		this.newsLetterSubscribed = newsLetterSubscribed;
		this.subscriptionDate = LocalDate.now();
	}

	public User() {
		this.subscriptionDate = LocalDate.now();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getRaisonSociale() {
		return raisonSociale;
	}

	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getNumTVAIntracom() {
		return numTVAIntracom;
	}

	public void setNumTVAIntracom(String numTVAIntracom) {
		this.numTVAIntracom = numTVAIntracom;
	}

	public String getSiretNumber() {
		return siretNumber;
	}

	public void setSiretNumber(String siretNumber) {
		this.siretNumber = siretNumber;
	}

	public int getEmployeeNumber() {
		return employeeNumber;
	}

	public void setEmployeeNumber(int employeeNumber) {
		this.employeeNumber = employeeNumber;
	}

	public boolean isNewsLetterSubscribed() {
		return newsLetterSubscribed;
	}

	public void setNewsLetterSubscribed(boolean newsLetterSubscribed) {
		this.newsLetterSubscribed = newsLetterSubscribed;
	}

	public LocalDate getSubscriptionDate() {
		return subscriptionDate;
	}

	public void setSubscriptionDate(LocalDate subscriptionDate) {
		this.subscriptionDate = subscriptionDate;
	}

	@Override
	public String toString() {
		return "User [raisonSociale=" + raisonSociale + ", siretNumber=" + siretNumber + "]";
	}
	
	
	
	

	
}
